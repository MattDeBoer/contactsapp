//
//  CreateAndUpdateViewController.swift
//  ContactsApp
//
//  Created by Matt DeBoer on 2/15/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit
import RealmSwift


class CreateAndUpdateViewController: UIViewController {
    
    @IBOutlet weak var firstNametext: UITextField!
    @IBOutlet weak var lastNametext: UITextField!
    @IBOutlet weak var phoneNumbertext: UITextField!
    @IBOutlet weak var addressText: UITextField!
    @IBOutlet weak var emaiText: UITextField!
    
    
    @IBAction func saveContact(_ sender: UIButton) {
        
    }

    var object:Category? = nil
    
    override func viewDidLoad() {
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: Selector("endEditing:")))

        super.viewDidLoad()
        
        
        if object != nil {
            // display the object
        }
        
        saveupdate()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
       // print("called")
        
        let realm = try! Realm()
        let category = Category()
        category.firstName = firstNametext.text ?? ""
        category.lastName = lastNametext.text ?? ""
        category.phoneNumber = phoneNumbertext.text ?? ""
        category.address = addressText.text ?? ""
        category.email = emaiText.text  ?? ""
        try! realm.write {
            realm.add(category)
        }
       // print(category)
    }
    
    func saveupdate() {
        
        let realm = try! Realm()
        
        if object == nil {
            //create the object
            let category = Category()
            category.firstName = ""
            category.lastName = ""
            category.phoneNumber = ""
            category.address = ""
            category.email = ""
            try! realm.write {
                realm.add(category)
            }
        } else {
            
            try! realm.write {
                object?.firstName = ""
                object?.lastName = ""
                object?.phoneNumber = ""
                object?.address = ""
                object?.email = ""
            }
            
            //update the object
        }
        
        
    }
    
    

        // Dispose of any resources that can be recreated.
    }
    
    


