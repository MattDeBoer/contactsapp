//
//  ViewController.swift
//  ContactsApp
//
//  Created by Matt DeBoer on 2/15/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    
    var categories:Results<Category>?
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        self.categories = realm.objects(Category.self)
        print(self.categories as Any)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let realm = try! Realm()
        categories = realm.objects(Category.self)
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "readOnlySegue", sender: categories![indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        let category = categories![indexPath.row]
        cell?.textLabel?.text = category.firstName
       // cell?.textLabel?.text = category.lastName
       // cell?.textLabel?.text = category.phoneNumber
       // cell?.textLabel?.text = category.address
       // cell?.textLabel?.text = category.email
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "readOnlySegue" {
            let dc = segue.destination as! ReadOnlyViewController
            dc.object = sender as? Category
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
