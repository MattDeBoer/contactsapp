//
//  ReadOnlyViewController.swift
//  ContactsApp
//
//  Created by Matt DeBoer on 2/15/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit
import MessageUI
import Foundation

class ReadOnlyViewController: UIViewController, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBAction func callNumber(_ sender: Any) {
        let phoneNumber:String = "tel://" + self.phoneNumberLabel.text!
        UIApplication.shared.openURL(NSURL(string: phoneNumber)! as URL)
        
    }
    
    
    @IBAction func textNumber(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()){
            let controller = MFMessageComposeViewController()
            controller.body = self.phoneNumberLabel.text!
            controller.recipients = [self.phoneNumberLabel.text!]
            controller.messageComposeDelegate = self as! MFMessageComposeViewControllerDelegate
            self.present(controller, animated: true, completion: nil)
        }
    }
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult){
        self.dismiss(animated: true, completion: nil)
    
    }
    @IBAction func emailLabel(_ sender: UIButton) {
        let mailComposeController = MFMailComposeViewController()
        mailComposeController.mailComposeDelegate = self;
        mailComposeController.setToRecipients(["blah@blah.com"])
        mailComposeController.setSubject("test")
        mailComposeController.setMessageBody("jjkkafasf", isHTML: false)
        
        
        if !MFMailComposeViewController.canSendMail() {
            self.present(mailComposeController, animated: true, completion:{ () -> Void in
                
            }
             )
    }
        func mailComposeController(controller: MFMailComposeViewController,
        didFinishWithResult result: MFMailComposeResult, error: NSError?) {
            controller.dismiss(animated: true, completion:{ () -> Void in
        }
         )
    }
        
    }
    var object:Category? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
       // print(object)
        
        let item = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editItem))
        self.navigationItem.rightBarButtonItem = item
        // Do any additional setup after loading the view.
        
        firstNameLabel.text = object?.firstName
        lastNameLabel.text = object?.lastName
        phoneNumberLabel.text = object?.phoneNumber
        addressLabel.text = object?.address
        emailLabel.text = object?.email
    }
    
    @objc func editItem() {
    performSegue(withIdentifier: "UpdateObjectSegue", sender: object)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UpdateObjectSegue" {
            let dc = segue.destination as! CreateAndUpdateViewController
            dc.object = object
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

