//
//  Category.swift
//  ContactsApp
//
//  Created by Matt DeBoer on 2/15/18.
//  Copyright © 2018 Matt DeBoer. All rights reserved.
//

import UIKit
import RealmSwift

class Category: Object {
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var phoneNumber = ""
    @objc dynamic var address = ""
    @objc dynamic var email = ""
}

